# Amiga Tanklight

This is an optical replacement board for the Amiga Tankmouse with
real quadrature signals


![Alt-Text](https://gitlab.com/marceljaehne/amiga-tanklight/-/raw/main/Pictures/23-11-03%2015-14-46%203993.jpg)

More informations: https://www.a1k.org/forum/index.php?threads/89564/#post-1729364




Original source: https://gitlab.com/marceljaehne/amiga-tanklight
